.TH SQ 1 0.34.0 "Sequoia PGP" "User Commands"
.SH NAME
sq pki \- Authenticate certs using the Web of Trust
.SH SYNOPSIS
.br
\fBsq pki authenticate\fR [\fIOPTIONS\fR] \fIFINGERPRINT|KEYID\fR \fIUSERID\fR
.br
\fBsq pki lookup\fR [\fIOPTIONS\fR] \fIUSERID\fR
.br
\fBsq pki identify\fR [\fIOPTIONS\fR] \fIFINGERPRINT|KEYID\fR
.br
\fBsq pki certify\fR [\fIOPTIONS\fR] \fICERTIFIER\-KEY\fR \fIKEY_ID|FINGERPRINT|FILE\fR \fIUSERID\fR
.br
\fBsq pki link\fR [\fIOPTIONS\fR]  \fISUBCOMMAND\fR
.br
\fBsq pki list\fR [\fIOPTIONS\fR] \fIPATTERN\fR
.br
\fBsq pki path\fR [\fIOPTIONS\fR] \fIFINGERPRINT|KEYID\fR \fIUSERID\fR
.SH DESCRIPTION
Authenticate certs using the Web of Trust.
.PP
The "Web of Trust" is a decentralized trust model popularized by PGP.
It is a superset of X.509, which is a hierarchical trust model, and is
the most popular trust model on the public internet today.  As used on
the public internet, however, X.509 relies on a handful of global
certification authorities (CAs) who often undermine its security.
.PP
The Web of Trust is more nuanced than X.509.  Using the Web of Trust,
require multiple, independent paths to authenticate a binding by only
partially trusting CAs.  This prevents a single bad actor from
compromising their security.  And those who have stronger security
requirements can use the Web of Trust in a completely decentralized
manner where only the individuals they select – who are not
necessarily institutions – act as trusted introducers.
.PP

.SH SUBCOMMANDS
.SS "sq pki authenticate"
Authenticate a binding.
.PP
Authenticate a binding (a certificate and User ID) by looking for a path from the trust roots to the specified binding in the Web of Trust.  Because certifications may express uncertainty (i.e., certifications may be marked as conveying only partial or marginal trust), multiple paths may be needed.
.PP
If a binding could be authenticated to the specified level (by default: fully authenticated, i.e., a trust amount of 120), then the exit status is 0.  Otherwise the exit status is 1.
.PP
If any valid paths to the binding are found, they are printed on stdout whether they are sufficient to authenticate the binding or not.
.PP

.SS "sq pki lookup"
Lookup the certificates associated with a User ID.
.PP
Identifies authenticated bindings (User ID and certificate pairs) where the User ID matches the specified User ID.
.PP
If a binding could be authenticated to the specified level (by default: fully authenticated, i.e., a trust amount of 120), then the exit status is 0.  Otherwise the exit status is 1.
.PP
If a binding could be partially authenticated (i.e., its trust amount is greater than 0), then the binding is displayed, even if the trust is below the specified threshold.
.PP

.SS "sq pki identify"
Identify a certificate.
.PP
Identify a certificate by finding authenticated bindings (User ID and certificate pairs).
.PP
If a binding could be authenticated to the specified level (by default: fully authenticated, i.e., a trust amount of 120), then the exit status is 0.  Otherwise the exit status is 1.
.PP
If a binding could be partially authenticated (i.e., its trust amount is greater than 0), then the binding is displayed, even if the trust is below the specified threshold.
.PP

.SS "sq pki certify"
Certify a User ID for a Certificate.
.PP
Using a certification a keyholder may vouch for the fact that another
certificate legitimately belongs to a user id.  In the context of
emails this means that the same entity controls the key and the email
address.  These kind of certifications form the basis for the Web of
Trust.
.PP
This command emits the certificate with the new certification.  The
updated certificate has to be distributed, preferably by sending it to
the certificate holder for attestation.  See also `sq key
attest\-certifications`.
.PP
By default a certification expires after 5 years.
Using the `\-\-expiry` argument specific validity periods may be defined.
It allows for providing a point in time for validity to end or a validity
duration.
.PP
`sq pki certify` respects the reference time set by the top\-level
`\-\-time` argument.  It sets the certification's creation time to the
reference time.
.PP


.SS "sq pki link"
Manage authenticated certificate and User ID links.
.PP
Link a certificate and User ID is one way of making `sq` consider a
binding to be authentic.  Another way is to use `sq pki certify` to
certify the binding with an explicitly configured trust root.  The
linking functionality is often easier to work with, and the
information is private by default.
.PP
Authenticated bindings can be used to designate a certificate using a
symbolic name.  For instance, using `sq encrypt`'s
`\-\-recipient\-userid` and `\-\-recipient\-email` options, a user can
designate a certificate using a User ID or an email address that is
authenticated for that certificate.
.PP
`sq` also uses authenticated certificates to authenticate other
data.  For instance, `sq verify` considers signatures made by an
authenticated certificate to be authentic.
.PP
Users can create a link using `sq pki link add`.  That link can later be
retracted using `sq pki link retract`.  A certificate can also be
accepted as a trusted introducer by passing the `\-\-ca` option to
`sq pki link add`.
.PP
`sq` implements linking using non\-exportable certifications, and an
implicit trust root.  An OpenPGP certificate directory, the default
certificate store used by `sq`, includes a local trust root, which
is stored under the `trust\-root` special name.  When the user
instructs `sq` to accept a binding, `sq` uses the local trust root
to create a non\-exportable certification, which it stores in the
certificate directory.  In this way, operations that use the Web of
Trust to authenticate a binding automatically use links.
.PP
When a user retracts a link, `sq` creates a new, non\-exportable
certification with zero trust.  This certification suppresses the
previous link.
.PP


.SS "sq pki list"
List all authenticated bindings (User ID and certificate pairs).
.PP
Only bindings that meet the specified trust amount (by default bindings that are fully authenticated, i.e., have a trust amount of 120), are shown.
.PP
Even if no bindings are shown, the exit status is 0.
.PP
If `\-\-email` is provided, then a pattern matches if it is a case insensitive substring of the email address as\-is or the normalized email address.  Note: unlike the email address, the pattern is not normalized.  In particular, puny code normalization is not done on the pattern.
.PP

.SS "sq pki path"
Verify the specified path.
.PP
A path is a sequence of certificates starting at the root, and a User ID.  This function checks that each path segment has a valid certification, which also satisfies any constraints (trust amount, trust depth, regular expressions).
.PP
If a valid path is not found, then this subcommand also lints the path.  In particular, it report if any certifications are insufficient, e.g., not enough trust depth, or invalid, e.g., because they use SHA\-1, but the use of SHA\-1 has been disabled.
.PP

.SH EXAMPLES
.SS "sq pki authenticate"
.PP

.PP
Authenticate a specific binding.
.PP
.nf
.RS
sq pki authenticate EB28F26E2739A4870ECC47726F0073F60FD0CBF0 \\
.RE
.RS
.RS
"Alice <alice@example.org>"
.RE
.RE
.PP
.fi

.PP
Check whether we can authenticate any user ID with the specified email
address for the given certificate.
.PP
.nf
.RS
sq pki authenticate EB28F26E2739A4870ECC47726F0073F60FD0CBF0 \\
.RE
.RS
.RS
\-\-email alice@example.org
.RE
.RE
.fi
.PP
.SS "sq pki lookup"
.PP

.PP
Lookup certificates that can be authenticated for the given user ID.
.PP
.nf
.RS
sq pki lookup "Alice <alice@example.org>"
.RE
.PP
.fi

.PP
Lookup certificates that have a user ID with the specified email
address, and that user ID can be authenticated.
.PP
.nf
.RS
sq pki lookup \-\-email alice@example.org
.RE
.fi
.PP
.SS "sq pki identify"
.PP

.PP
Identify the user IDs that can be authenticated for the certificate.
.PP
.nf
.RS
sq pki identify EB28F26E2739A4870ECC47726F0073F60FD0CBF0
.RE
.PP
.fi

.PP
List all user IDs that have that have been certified by anyone.
.PP
.nf
.RS
sq pki identify \-\-gossip \\
.RE
.RS
.RS
511257EBBF077B7AEDAE5D093F68CB84CE537C9A
.RE
.RE
.fi
.PP
.SS "sq pki certify"
.PP

.PP
Juliet certifies that Romeo controls romeo.pgp and romeo@example.org
.PP
.nf
.RS
sq pki certify juliet.pgp romeo.pgp '<romeo@example.org>'
.RE
.PP
.fi

.PP
Certify the User ID Ada, and set the certification time to July
21, 2013 at midnight UTC:
.PP
.nf
.RS
sq pki certify \-\-time 20130721 neal.pgp ada.pgp Ada
.RE
.fi
.PP
.SS "sq pki list"
.PP

.PP
List all bindings for user IDs containing an email address from
example.org, and that can be authenticated.
.PP
.nf
.RS
sq pki list @example.org
.RE
.fi
.PP
.SS "sq pki path"
.PP

.PP
Verify that Alice ceritified a particular User ID for Bob's certificate.
.PP
.nf
.RS
sq pki path EB28F26E2739A4870ECC47726F0073F60FD0CBF0 \\
.RE
.RS
.RS
511257EBBF077B7AEDAE5D093F68CB84CE537C9A \\
.RE
.RE
.RS
.RS
"Bob <bob@example.org>"
.RE
.RE
.fi
.SH "SEE ALSO"
.nh
\fBsq\fR(1), \fBsq\-pki\-authenticate\fR(1), \fBsq\-pki\-lookup\fR(1), \fBsq\-pki\-identify\fR(1), \fBsq\-pki\-certify\fR(1), \fBsq\-pki\-link\fR(1), \fBsq\-pki\-list\fR(1), \fBsq\-pki\-path\fR(1).
.hy
.PP
For the full documentation see <https://book.sequoia\-pgp.org>.
.SH VERSION
0.34.0 (sequoia\-openpgp 1.19.0)
