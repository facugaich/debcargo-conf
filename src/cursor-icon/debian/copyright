Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cursor-icon
Upstream-Contact: Kirill Chibisov <contact@kchibisov.com>
Source: https://github.com/rust-windowing/cursor-icon

Files: *
Copyright: 2023 Kirill Chibisov <contact@kchibisov.com>
License: MIT or Apache-2.0 or Zlib

Files: src/lib.rs
Copyright:
 2018, 2023 W3C® (MIT, ERCIM, Keio, Beihang)
License: W3C
Comment:
 License declaration in the file:
 .
 This file contains a portion of the CSS Basic User Interface Module Level 3
 specification. In particular, the names for the cursor from the #cursor
 section and documentation for some of the variants were taken.
 .
 The original document is https://www.w3.org/TR/css-ui-3/#cursor.

Files: debian/*
Copyright:
 2023 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2023 Blair Noctis <n@sail.ng>
License: MIT or Apache-2.0 or Zlib

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

License: W3C
 License
 .
 By obtaining and/or copying this work, you (the licensee) agree that you have
 read, understood, and will comply with the following terms and conditions.
 .
 Permission to copy, modify, and distribute this work, with or without
 modification, for any purpose and without fee or royalty is hereby granted,
 provided that you include the following on ALL copies of the work or portions
 thereof, including modifications:
 .
 - The full text of this NOTICE in a location viewable to users of the
   redistributed or derivative work.
 - Any pre-existing intellectual property disclaimers, notices, or terms and
   conditions. If none exist, the W3C Software and Document Short Notice
   should be included.
 - Notice of any changes or modifications, through a copyright statement on
   the new code or document such as "This software or document includes
   material copied from or derived from [title and URI of the W3C document].
   Copyright © [YEAR] W3C® (MIT, ERCIM, Keio, Beihang)."
 .
 Disclaimers
 .
 THIS WORK IS PROVIDED "AS IS," AND COPYRIGHT HOLDERS MAKE NO REPRESENTATIONS
 OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO, WARRANTIES
 OF MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF
 THE SOFTWARE OR DOCUMENT WILL NOT INFRINGE ANY THIRD PARTY PATENTS,
 COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS.
 .
 COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL OR
 CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE OF THE SOFTWARE OR DOCUMENT.
 .
 The name and trademarks of copyright holders may NOT be used in advertising
 or publicity pertaining to the work without specific, written prior
 permission. Title to copyright in this work will at all times remain with
 copyright holders.
